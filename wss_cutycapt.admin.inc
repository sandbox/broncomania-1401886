<?php
 
/**
 * Implementation of hook_configure().
 *
 * @todo convert in t() functions with parameters
 */
 
function wss_cutycapt_settings(&$form_state) {
  $form = array();
  
  $form['website_screenshot_cutycapt_testsnapshot'] = array(
    '#type' => 'textfield',
    '#title' => t('CutyCapt snapshot test'),
    '#default_value' => isset($url)?$url : 'http://',
    '#description' => t('Test the snapshot engine.'),
    '#prefix' => '<div><table><tr><td width="70%">',
    '#suffix' => '</td>'
  );  

  $form['website_screenshot_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Make screenshot'),
    '#prefix' => '<td style="vertical-align:middle;">',
    '#suffix' => '<td/></tr></table></div>'
  );
  
  $form['website_screenshot_cutycapt_home'] = array(
    '#type' => 'textfield',
    '#title' => t('CutyCapt home folder'),
    '#default_value' => variable_get('website_screenshot_cutycapt_home', ""),
    '#description' => t('The path to the CutyCapt installation.')
  );
  $form['website_screenshot_cutycapt_delay'] = array(
    '#type' => 'textfield',
    '#title' => t('CutyCapt delay'),
    '#default_value' => variable_get('website_screenshot_cutycapt_delay', "3000"),
    '#description' => t('The snapshot delay is the time time cutycapt waits for the website to load completely.')
  );
  $form['website_screenshot_cutycapt_user_agent'] = array(
    '#type' => 'textfield',
    '#title' => t('CutyCapt user agent'),
    '#default_value' => variable_get('website_screenshot_cutycapt_user_agent', "Firefox"),
    '#description' => t('The user agent for the snapshots. Mozilla,Firefox,IE,...')
  );

  $form['website_screenshot_cutycapt_refresh'] = array(
    '#type' => 'textfield',
    '#title' => t('CutyCapt refresh period'),
    '#default_value' => variable_get('website_screenshot_cutycapt_refresh_period', 10080),
    '#description' => t('When should the screenshot be refreshed. After x milliseconds')
  );

  $imagetyp = array('jpg' => 'jpg', 'png' => 'png'); 
  $form['website_screenshot_cutycapt_imagetype'] = array(
    '#type' => 'select',
    '#title' => t('CutyCapt imagetype'),
    '#default_value' => variable_get('website_screenshot_cutycapt_imagetype', "jpg"),
    '#options' => $imagetyp,
    '#description' => t('What kind of snapshot should be made jpg or png')
  ); 
  
  $form['#submit'][] = 'website_screenshot_cutycapt_submit';
  
  return system_settings_form($form);
}


function website_screenshot_cutycapt_submit($form, &$form_state){
 if($form_state['clicked_button']['#post']['op'] == t('Make screenshot') ){
    $url = $form_state['values']['website_screenshot_cutycapt_testsnapshot'];
    if(valid_url($url)){
      $cutycapt_path = cutycapt($url);
      $image = theme('imagecache', 'screenshot',$cutycapt_path, $url,$url);
      drupal_set_message(t('Your snapshot:' . $image. ' path:'.$cutycapt_path), 'status');
      return $image;
    }else{
      drupal_set_message(t('Missing URL to grab website screenshot.' . $url), 'error');
    }
 }
} 
